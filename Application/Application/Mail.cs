﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application
{
    public class Mail
    {
        private Infraestructure.Abstract.IMail mail;

        public Mail(Infraestructure.Abstract.IMail _mail)
        {
            this.mail = _mail;
        }

        public bool SendMail(string subject, string message, string from, string to)
        {
            return this.mail.SendMail(subject, message, from, to);
        }

        public string GetMailSender()
        {
            return this.mail.GetMailSender();
        }
    }
}
