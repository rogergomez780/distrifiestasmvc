﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application
{
    public class Product
    {
        private Infraestructure.Abstract.IProduct productRepository;

        public Product(Infraestructure.Abstract.IProduct product)
        {
            this.productRepository = product;
        }

        public IList<Domain.Product> GetProducts(Domain.Line line)
        {
            return this.productRepository.GetProducts(line);
        }

        public IList<Domain.Reference> GetReferences(Domain.Product product)
        {
            return this.productRepository.GetReferences(product);
        }
    }
}
