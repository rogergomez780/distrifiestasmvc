﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class ProductOrder
    {
        public Reference Reference { get; set; }
        public int Quantity { get; set; }
        public decimal Total { get; set; }
        public string Notes { get; set; }
    }
}
