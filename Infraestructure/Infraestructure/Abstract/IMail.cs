﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;

namespace Infraestructure.Abstract
{
    public interface IMail
    {
        bool SendMail(string subject, string message, string from, string to);
        string GetMailSender();
    }
}
