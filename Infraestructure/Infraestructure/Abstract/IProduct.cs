﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;

namespace Infraestructure.Abstract
{
    public interface IProduct
    {
        IList<Product> GetProducts(Line line);

        IList<Reference> GetReferences(Product product);
    }
}
