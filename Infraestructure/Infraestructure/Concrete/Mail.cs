﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Infraestructure.Concrete
{
    public class Mail : Abstract.IMail
    {
        public bool SendMail(string subject, string message, string from, string to)
        {
            try
            {
                SmtpClient cliente = new SmtpClient();
                cliente.Host = ConfigurationManager.AppSettings["mailServer"];
                cliente.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["userMail"], ConfigurationManager.AppSettings["mailPassword"]);
                MailMessage mensaje = new MailMessage();
                mensaje.From = new MailAddress(from);
                mensaje.To.Add(new MailAddress(to));
                mensaje.IsBodyHtml = true;
                mensaje.Subject = subject;
                mensaje.Body = GetHtmlTextMessage(message);
                cliente.Send(mensaje);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public string GetMailSender()
        {
            return ConfigurationManager.AppSettings["userMail"];
        }

        private string GetHtmlTextMessage(string message)
        {
            string templatePath = ConfigurationManager.AppSettings["templatePath"];
            StringBuilder stringReplace = new StringBuilder();
            using (StreamReader reader = new StreamReader(templatePath))
            {
                stringReplace.Append(reader.ReadToEnd());
            }
            return stringReplace.Replace("@messageText", message).ToString();
        }

        
    }
}
