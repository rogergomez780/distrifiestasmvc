﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Infraestructure.Concrete
{
    public class Product : Abstract.IProduct
    {
        private static string IMAGENNODISPONIBLE = "~/images/notAvailable.gif";

        public IList<Domain.Product> GetProducts(Domain.Line line)
        {
            XElement element = Util.UtilFunctionsXML.GetDocument(line);
            var products = element.Descendants("Producto").ToList();
            List<Domain.Product> productsResult = new List<Domain.Product>();
            foreach (var product in products)
            {
                productsResult.Add(new Domain.Product()
                {
                    Id = int.Parse(product.Element("IdProducto").Value),
                    Name = product.Element("NombreProducto").Value,
                    ImagePath = string.IsNullOrEmpty(product.Element("RutaImagen").Value) ? IMAGENNODISPONIBLE : product.Element("RutaImagen").Value,
                    Line = line
                });
            }

            return productsResult.ToList();
        }

        public IList<Domain.Reference> GetReferences(Domain.Product product)
        {
            XElement element = Util.UtilFunctionsXML.GetDocument(product.Line);
            var products = element.Descendants("Producto").ToList();
            List<Domain.Reference> referencesResult = new List<Domain.Reference>();
            int productId;
            int referenceId = 0;
            foreach (var productItem in products)
            {
                productId = int.Parse(productItem.Element("IdProducto").Value);
                if (productId.Equals(product.Id))
                {
                    var references = productItem.Descendants("Referencia").ToList();
                    foreach (var reference in references)
	                {
		                referencesResult.Add(new Domain.Reference()
                            {
                                Id = referenceId,
                                Name = reference.Attribute("NombreReferencia").Value,
                                Value = decimal.Parse(reference.Attribute("Valor").Value),
                                ImagePath = string.IsNullOrEmpty(reference.Attribute("RutaImagen").Value) ? IMAGENNODISPONIBLE : reference.Attribute("RutaImagen").Value
                            });
                        referenceId++;
	                }
                }
                
            }
            return referencesResult.ToList();
        }
    }
}
