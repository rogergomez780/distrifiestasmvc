﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Infraestructure.Util
{
    public class UtilFunctionsXML
    {
        public static XElement GetDocument(Domain.Line line)
        {
            List<string> fileList = Util.Files.GetFileList();

            string name = fileList.FirstOrDefault(x => x.ToUpper().Contains(line.Name.ToUpper())).ToString();
            string pathData = System.Configuration.ConfigurationManager.AppSettings["PathData"];
            XElement xEle = XElement.Load(string.Format(@"{0}\{1}", pathData, name));
            return xEle;
        }
    }
}
