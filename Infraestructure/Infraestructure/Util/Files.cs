﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Infraestructure.Util
{
    public class Files
    {
        private static List<string> fileList = new List<string>() { "ProductosBolsas.xml",
                                               "ProductosDesechables.xml",
                                               "ProductosIcopor.xml",
                                               "ProductosPiniateria.xml",
                                               "ProductosVarios.xml"
                                             };

        public static List<string> GetFileList()
        {
            return fileList;
        }
    }
}
