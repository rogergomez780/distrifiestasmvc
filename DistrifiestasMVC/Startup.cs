﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TestAzureEFAng.Startup))]
namespace TestAzureEFAng
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
