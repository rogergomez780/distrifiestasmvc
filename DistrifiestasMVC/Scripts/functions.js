﻿$(document).ready(function ($) {
    $('.txtNumeric').autoNumeric('init');
});

var messageInserted = "Producto agregado a la bolsa de compras";
var messageUpdated = "Producto actualizado en la bolsa de compras";
var messageRemoved = "Producto eliminado de la bolsa de compras";

var MessageTypes = 
{
    Warning: 10,
    Error: 20,
    Information: 30
}

function Calculate(quantityControl, value, id) {
    var quantity = quantityControl.value.replace(".", "");
    CalculateValue(quantity, value, id);
}

function CalculateValue(quantity, value, id) {
    var total = quantity * value;
    $("#" + id).html("Total: $" + total);
}

function AddItem(AddButtonControl, value, item, productLine, productID) {
    var referenceID = AddButtonControl.name;
    var quantity = $("#txtQuantity-" + referenceID).val().replace(".", "");
    var note = $("#txtObservacion-" + referenceID).val();

    var jsonProduct = { ReferenceID: referenceID, Quantity: quantity, Value: value, ReferenceName: item, Note: note, Line: productLine, ProductID: productID };

    var storage = $.localStorage;
    var productList = storage.get('productList');
    if (productList == null)
    {
        productList = [];
        productList.push(jsonProduct);
        storage.set('productList', productList);

        ShowMessage(messageInserted, MessageTypes.Information);
    }
    else
    {
        var exists = false;
        productList.forEach(function (element, index) {
            if (element.ReferenceID == jsonProduct.ReferenceID && element.ProductID == jsonProduct.ProductID)
            {
                productList[index] = jsonProduct;
                exists = true;
                ShowMessage(messageUpdated, MessageTypes.Information);
            }
        });

        if (!exists)
        {
            productList.push(jsonProduct);
            ShowMessage(messageInserted, MessageTypes.Information);
        }
        storage.set('productList', productList);
    }
}

function RemoveItem(RemoveButtonControl, productLine, productID)
{
    var referenceID = RemoveButtonControl.name;
    var storage = $.localStorage;
    var productList = storage.get('productList');
    if (productList != null) {
        var exists = false;
        productList.forEach(function (element, index) {
            if (element.Line == productLine && element.ProductID == productID && element.ReferenceID == referenceID) {
                exists = true;
                productList.splice(index, 1);
            }
        });

        if (exists) {
            storage.set('productList', productList);
            $("#txtQuantity-" + referenceID).val("");
            $("#txtObservacion-" + referenceID).val("");
            CalculateValue(0, 0, referenceID);
            ShowMessage(messageRemoved);
        }
        else {
            ShowMessage("No posee este producto en su lista");
        }
    }
    else {
        ShowMessage("No posee productos para eliminar" )
    }
}

function LoadInformation(line, productID)
{
    var storage = $.localStorage;
    var productList = storage.get('productList');
    if (productList != null) {
        var exists = false;
        productList.forEach(function (element, index) {
            if (element.Line == line && element.ProductID == productID) {
                $("#txtQuantity-" + element.ReferenceID).val(element.Quantity);
                $("#txtObservacion-" + element.ReferenceID).val(element.Note);
                CalculateValue(element.Quantity, element.Value, element.ReferenceID);
            }
        });
    }
}

function ShowMessage(message, type)
{
    // Type parameter will be used once a mechanism for showing alerts is implemented
    $("#MessagePanel").html(message);
    $("#MessagePanel").modal();
    //alert(message);
}

