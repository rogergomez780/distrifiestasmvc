﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Application;
using Infraestructure.Abstract;
using Domain;

namespace TestAzureEFAng.Controllers
{
    public class OrdersController : Controller
    {
        Application.Product product;
        Application.Mail mail;

        public OrdersController()
        {
        }

        public OrdersController(IProduct _product, IMail _mail)
        {
            product = new Application.Product(_product);
            mail = new Mail(_mail);
        }

        public ActionResult Orders()
        {
            List<ProductOrder> productsOrdered = new List<ProductOrder>();
            productsOrdered.Add(new ProductOrder() { Reference = new Reference() { Description = "My reference" }, Quantity = 3, Total = 4500 });
            productsOrdered.Add(new ProductOrder() { Reference = new Reference() { Description = "My reference2" }, Quantity = 3, Total = 7000 });

            return View(productsOrdered);
        }
    }
}