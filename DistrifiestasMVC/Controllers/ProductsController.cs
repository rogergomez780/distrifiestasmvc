﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Application;
using Infraestructure.Abstract;

namespace TestAzureEFAng.Controllers
{
    public class ProductsController : Controller
    {
        Application.Product product;
        Application.Mail mail;

        public ProductsController()
        {
        }

        public ProductsController(IProduct _product, IMail _mail)
        {
            product = new Product(_product);
            mail = new Mail(_mail);
        }

        public ActionResult Lines(string line = "Icopor")
        {
            Domain.Line lineProduct = new Domain.Line();
            lineProduct.Name = line;
            ViewBag.MailSender = mail.GetMailSender();
            return View(product.GetProducts(lineProduct));
        }

        public ActionResult Products(string line, string productID)
        {
            Domain.Product productFilter = new Domain.Product();
            productFilter.Id = int.Parse(productID);
            productFilter.Line = new Domain.Line() { Name = line };
            return View(product.GetReferences(productFilter));
        }

    }
}