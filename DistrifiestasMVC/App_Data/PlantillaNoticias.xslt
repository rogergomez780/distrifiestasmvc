﻿<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>
  <xsl:template match="/">
    <Noticia xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
      <xsl:for-each select="Noticias/Noticia">
        <span class="smallredtext">
          <xsl:value-of select="Fecha" />
        </span>
        <br />
        <span class="bodytext">
          <xsl:value-of select="Texto" />
        </span>
        <br />
        <a href="#{LinkMas}" class="smallgraytext">
          <xsl:value-of select="LinkMas" />
        </a>
        <br />
      </xsl:for-each>
    </Noticia>
  </xsl:template>
</xsl:stylesheet>